package programmes;

import java.util.Random;
import java.util.Scanner;

public class Demo4 {

    public static void main(String[] args) {
       
        Scanner  scan   = new Scanner(System.in);
       
        boolean  trouve = false;
        
        int nbADeviner;
        int nbPropose;
        int nbEssais=0;
      
       
        Random rd=new Random();
        nbADeviner=rd.nextInt(10)+1;  
        
        while ( ! trouve ){
        
          nbEssais++;
            
          System.out.println("Proposez un nombre entre 1 et 10");
          nbPropose=scan.nextInt();
        
          if(nbPropose == nbADeviner){
        
            System.out.println("Vous avez trouvé en "+nbEssais+" Essais!");
            trouve=true;
          }
          else{ System.out.println("vous n'avez pas deviné!");}
        
        }
    }
}

